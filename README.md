# TU Dortmund slides template

This repository contains a LaTeX template for slides in TU Dortmund 
corporate design. It is a remix from https://www.LaTeXTemplates.com and
a TU Dortmund theme for the beamer LaTeX class from 
https://git.cs.tu-dortmund.de/vincent.reckendrees/antritts-vortrag.

## Usage

Keep the folder structure as provided to make it work out of the box.
Simply adjust main.tex as you wish to create your slides for presentations
at TU Dortmund. This template uses the LaTeX beamer class as mentioned.
For a quick start look [here](https://de.overleaf.com/learn/latex/Beamer) and for 
detailed documentation [here](https://mirror.clientvps.com/CTAN/macros/latex/contrib/beamer/doc/beameruserguide.pdf).
This template should also work with Overleaf. Compile with pdflatex.

## License

Licensed under MIT license.

---
Contributor: [Maximilian Krebs](https://gitlab.com/printerboi)
